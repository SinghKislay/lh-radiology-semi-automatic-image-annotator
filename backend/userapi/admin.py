from django.contrib import admin
from .models import *
# Register your models here.
admin.site.register(Xray)
admin.site.register(Bbox)
admin.site.register(Segment)
admin.site.register(UserProfile)
admin.site.register(DLModel)
admin.site.register(Disease)
admin.site.register(Log)

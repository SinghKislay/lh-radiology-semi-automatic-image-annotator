"""This module is responsible for declaration of serializers"""
from rest_framework import serializers
from drf_extra_fields.fields import Base64ImageField
from userapi.models import *

class UserProfileSerializer(serializers.ModelSerializer):
    """
    This class will represent serializer for UserProfile model
    """
    class Meta:
        model = UserProfile
        fields = ('__all__')

class DLModelSerializer(serializers.ModelSerializer):
    """
    This class will represent serializer for Deep learning model
    """
    class Meta:
        model = DLModel
        fields = ('__all__')

class DiseaseSerializer(serializers.ModelSerializer):
    """
    This class will represent serializer for Disease
    """
    class Meta:
        model = Disease
        fields = ('__all__')

class LogSerializer(serializers.ModelSerializer):
    """
    This class will represent serializer for Logs people who edited diseases
    """
    disease = serializers.CharField()
    class Meta:
        model = Log
        fields = ('disease', )

    def create(self, validated_data):
        user_instance = UserProfile.objects.filter(user=self.context['request'].user)[0]
        disease_name = validated_data.pop('disease')
        disease_instance = Disease.objects.get_or_create(name=disease_name)[0]
        Log_instance = Log.objects.create(disease=disease_instance, username=user_instance)
        return Log_instance

class BboxSerializer(serializers.ModelSerializer):
    """
    This class will represent serializer for Bbox model
    """
    dl_model = serializers.CharField()
    disease = serializers.CharField()

    class Meta:
        model = Bbox
        fields = ('id', 'xmin', 'ymin', 'xmax', 'ymax', 'disease', 'dl_model')

    def create(self, validated_data):
        """
        Creates the Bbox model with dl_model.
        """
        dl_model_name = validated_data.pop('dl_model')
        dl_model_instance, created = DLModel.objects.get_or_create(name=dl_model_name)
        disease_name = validated_data.pop('disease')
        
        disease_instance = Disease.objects.get_or_create(name=disease_name)[0]
        
        bbox_instance = Bbox.objects.create(**validated_data, dl_model=dl_model_instance, disease=disease_instance)
        return bbox_instance

class XrayBboxSerializer(serializers.ModelSerializer):
    """
    This class will represent nested serializer for Xray and Bbox model
    """
    picture = Base64ImageField()
    segment = Base64ImageField()
    bboxes = BboxSerializer(many=True)

    class Meta:
        model = Xray
        fields = ('id', 'bboxes', 'picture', 'pic_name', 'user_profile', 'expert_check', 'segment', 'height', 'width')

    def create(self, validated_data):
        """
        Creates the Xray model with Bboxes.
        """
        bboxes_val_data = validated_data.pop('bboxes')
        xray = Xray.objects.create(**validated_data)
        bbox_set_serialiser = self.fields['bboxes']
        for bbox in bboxes_val_data:
            bbox['xray'] = xray
        bboxes = bbox_set_serialiser.create(bboxes_val_data)
        return xray

    def update(self, instance, validated_data):
        """
        Updates the Bbox data of xray and which user has updated it
        """
        if(DLModel.objects.filter(name="Human")):
            bb = Bbox.objects.filter(xray=instance, dl_model=DLModel.objects.filter(name="Human")[0]).delete()
        bboxes = validated_data['bboxes']
        bbox_set_serialiser = self.fields['bboxes']
        for bbox in bboxes:
            bbox["xray"] = instance
        bbox_set_serialiser.create(bboxes)
        instance.user_profile = UserProfile.objects.filter(user=self.context['request'].user)[0]
        instance.expert_check = 1
        instance.save()
        return instance

class SegmentSerializer(serializers.ModelSerializer):
    """
    This class will represents the serializer for Segment model
    """
    class Meta:
        model = Segment
        fields = ('__all__')

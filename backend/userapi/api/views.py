"""This module is responsible for Views of diiferent requests"""
import random
from rest_framework import generics, views, status
from rest_framework.response import Response
from userapi.models import *
from .serializers import *
from django.http import HttpResponse
import csv

class XrayCreateView(views.APIView):
    """
    This class will represent view for creation Xray model object
    """
    def post(self, request, *args, **kwargs):
        """
        This method handles the post request
        """
        serializer = XrayBboxSerializer(data=request.data)
        if serializer.is_valid():
            xray = serializer.save()
            serializer = XrayBboxSerializer(xray)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class CSVView(views.APIView):
    """
    This class will represent view for creation of CSV File.
    """
    def get(self, request, *args, **kwargs):
        xrays = Xray.objects.all()
        feature_list = []
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
        writer = csv.writer(response)
        writer.writerow(['filename', 'xmin', 'xmax', 'ymin', 'ymax', 'disease', 'annotator', 'height', 'width'])
        for xray in xrays:
            bboxes = Bbox.objects.filter(xray=xray)
            for bbox in bboxes:
                name = xray.pic_name
                xmin = bbox.xmin
                xmax = bbox.xmax
                ymin = bbox.ymin
                ymax = bbox.ymax
                disease = bbox.disease
                model = bbox.dl_model.name
                height = xray.height
                width = xray.width
                writer.writerow([name, xmin, xmax, ymin, ymax, disease, model, height, width])
        return response

class SegmentCSVView(views.APIView):
    """
    This class will represent view for creation of CSV File.
    """
    def get(self, request, *args, **kwargs):
        xrays = Xray.objects.all()
        feature_list = []
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
        writer = csv.writer(response)
        writer.writerow(['filename', 'points', 'heigth', 'width', 'disease'])
        for xray in xrays:
            segments = Segment.objects.filter(xray=xray)
            if(segments):
                segments = segments[0]
                name = xray.pic_name
                points = segments.points
                height = xray.height
                width = xray.width
                disease = "Pneumothorax"
                writer.writerow([name, points, height, width, disease])
        return response

class BboxUpdateView(generics.UpdateAPIView):
    """
    This class will represent view for Updation of Bbox data.
    """
    serializer_class = XrayBboxSerializer
    queryset = Xray.objects.all()

class XrayListView(generics.ListAPIView):
    """
    This class will represent view for listing all Xrays that has not
    been reviewed by experts
    """
    serializer_class = XrayBboxSerializer
    
    def get_queryset(self):
        """
        This method returns the queryset
        """
        obj = Xray.objects.filter(expert_check=0)
        if len(obj) > 1:
            indx = random.randint(0, len(obj)-1)
            _id = obj[indx].id
            return Xray.objects.filter(id=_id)
        else:
            return Xray.objects.filter(expert_check=0)

class UserProfileView(generics.ListAPIView):
    """
    This class will represent view for getting user info.
    """
    serializer_class = UserProfileSerializer
    def get_queryset(self):
        """
        This method returns the queryset
        """
        return UserProfile.objects.filter(user=self.request.user) 

class BboxGetView(generics.ListAPIView):
    """
    This class will represent view for getting Bbox data of specified Deep Learning Model.
    """
    serializer_class = BboxSerializer
    def get_queryset(self):
        """
        This method returns the queryset
        """
        xray = self.kwargs['pk']
        model = self.request.query_params.get("model")
        model = DLModel.objects.filter(name=model)
        if(model):
            return Bbox.objects.filter(xray=xray).filter(dl_model=model[0])
        return []
  
class DLModelGetView(generics.ListAPIView):
    """
    This class will represent view for getting all the dl models.
    """
    serializer_class = DLModelSerializer
    queryset = DLModel.objects.exclude(name="Human")

class DiseaseGetView(generics.ListAPIView):
    """
    This class will represent view for getting all the diseases.
    """
    serializer_class = DiseaseSerializer
    queryset = Disease.objects.all()

class LogsPostView(generics.ListCreateAPIView):
    """
    This class will represent view for creating and listing logs.
    """
    serializer_class = LogSerializer
    queryset = Log.objects.all()

class SegmentView(generics.ListCreateAPIView):
    """
    This class will represent view for creatign and listing segmentation points.
    """
    serializer_class = SegmentSerializer
    queryset = Segment.objects.all()

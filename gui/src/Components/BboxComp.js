import React, { useState, useRef, useEffect } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

function BboxComp(props) {
  // Declare a new state variable, which we'll call "count"
  const [clicked, setClicked] = useState(0);
  const [disease, setDisease] = useState('');

  const [offset, setOffset] = useState({x:0, y:0});
  const [dotpos, setDotpos] = useState({x:0, y:0});

  const handleChange = (event) => {
    setDisease(event.target.value);
  };

  let cordObj = {...props.cords}
  useEffect(() => {
    if(props.clickedOut){
      setClicked(0)
    }
  })
  const inputRef = useRef();
  return (
     !clicked ?
      <div onClick={(e)=>{
        e.stopPropagation();
        setClicked(1);
        props.clickIn()
      }}
      
      style={{ position:'absolute',
      
      height:cordObj.bbh,
      width:cordObj.bbw,
     backgroundColor:"rgba(255, 0, 0, 0)",
     border: "solid 5px #6beb34",
     zIndex: 10,
     transform: `translate(${cordObj.x}px, ${cordObj.y}px)`,
     
      }}
      >
        <div style={{color:'white'}}>
          {cordObj.name}
        </div>
    </div>
    :
    

    <div id='bbox' draggable
    onDragStart={(e) => {
      setOffset({x:e.clientX - cordObj.x, y:e.clientY - cordObj.y});
    }}

    onDragEnd={(e) => {
    props.setCord(e.clientX - offset.x, e.clientY - offset.y, props.id);
    setDotpos({x:e.clientX, y:e.clientY});
    }}

    onMouseUp ={(e) => {
      const sx = document.getElementById('bbox').clientWidth;
      const sy = document.getElementById('bbox').clientHeight;
      props.setSize(sx, sy, props.id);
      
    }}

     style={{ position:'absolute',
     height:cordObj.bbh,
     width:cordObj.bbw,
     backgroundColor:"rgba(255, 0, 0, 0)",
     border: "solid 5px #F00",
     transform: `translate(${cordObj.x}px, ${cordObj.y}px)`,
     zIndex: 10,
     resize:'both',
     overflow: 'hidden',
     minHeight: 30,
     minWidth: 30,
      }}
    >
      
      

      <InputLabel id="demo-simple-select-label">Diseases</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={disease}
          onChange={(e) => {handleChange(e)
            props.setName(e.target.value, props.id)
          }}
          ref={inputRef}
        >
          {props.diseases.map((value, index) => (<MenuItem key={index} value={value.name}>{value.name}</MenuItem>))}
        </Select>


    </div>
  );
}

export default BboxComp
